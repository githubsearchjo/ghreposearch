package com.mobicubes.ghreposearch.domain.interactor;

import android.support.annotation.NonNull;

import com.mobicubes.ghreposearch.domain.entity.UserItem;
import com.mobicubes.ghreposearch.domain.repository.Repository;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by kuba on 10/03/2018.
 */

public class SearchUsersUseCase extends BaseUseCase<String, List<UserItem>> {

    @NonNull
    private final Repository repository;

    public SearchUsersUseCase(
            @NonNull final Scheduler subscribeOnScheduler,
            @NonNull final Scheduler observeOnScheduler,
            @NonNull final Repository repository
    ) {
        super(subscribeOnScheduler, observeOnScheduler);
        this.repository = repository;
    }

    @Override
    protected Observable<List<UserItem>> buildObservable(@NonNull final String query) {
        return repository.searchUsers(query);
    }
}
