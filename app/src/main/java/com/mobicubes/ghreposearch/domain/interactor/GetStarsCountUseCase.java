package com.mobicubes.ghreposearch.domain.interactor;

import android.support.annotation.NonNull;

import com.mobicubes.ghreposearch.domain.repository.Repository;

import io.reactivex.Observable;
import io.reactivex.Scheduler;

/**
 * Created by kuba on 12/03/2018.
 */

public class GetStarsCountUseCase extends BaseUseCase<String, Long> {

    @NonNull
    private final Repository repository;

    public GetStarsCountUseCase(
            @NonNull final Scheduler subscribeOnScheduler,
            @NonNull final Scheduler observeOnScheduler,
            @NonNull final Repository repository
    ) {
        super(subscribeOnScheduler, observeOnScheduler);
        this.repository = repository;
    }

    @Override
    protected Observable<Long> buildObservable(String login) {
        return repository.getUserRepositoriesStarCount(login);
    }
}
