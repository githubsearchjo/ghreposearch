package com.mobicubes.ghreposearch.domain.entity.mock;

import com.mobicubes.ghreposearch.domain.entity.UserItem;

/**
 * Created by kuba on 28/03/2018.
 */

public final class UserItemMock {

    private UserItemMock() {
        //no-op
    }

    public static UserItem.Builder get() {
        return UserItem.newBuilder()
                .withId(2345L)
                .withAvatarUrl("http://avatar.url.jpg")
                .withLogin("login");
    }
}
