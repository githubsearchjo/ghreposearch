package com.mobicubes.ghreposearch.domain.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.Scheduler;
import io.reactivex.disposables.Disposable;
import io.reactivex.disposables.Disposables;

/**
 * Created by kuba on 10/03/2018.
 */

public abstract class BaseUseCase<P, R> {

    @NonNull protected final Scheduler subscribeOnScheduler;
    @NonNull protected final Scheduler observeOnScheduler;

    public BaseUseCase(
            @NonNull final Scheduler subscribeOnScheduler,
            @NonNull final Scheduler observeOnScheduler
    ) {
        this.subscribeOnScheduler = subscribeOnScheduler;
        this.observeOnScheduler = observeOnScheduler;
    }

    @NonNull
    private Disposable disposable = Disposables.empty();
    @Nullable
    private Observable<R> observable;

    public <O extends Observer<R> & Disposable> void execute(
            final P param,
            final O resultObserver
    ) {
        if (isExecuting()) {
            unsubscribe();
        }

        disposable = resultObserver;

        observable = buildObservable(param)
                .subscribeOn(subscribeOnScheduler)
                .observeOn(observeOnScheduler)
                .doOnTerminate(() -> observable = null);

        observable.subscribe(resultObserver);
    }

    public boolean isExecuting() {
        return observable != null;
    }

    public void unsubscribe() {
        if (!disposable.isDisposed()) {
            disposable.dispose();
            disposable = Disposables.empty();
        }
        observable = null;
    }

    protected abstract Observable<R> buildObservable(P param);
}
