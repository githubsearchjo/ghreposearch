package com.mobicubes.ghreposearch.domain.entity.mock;

import com.mobicubes.ghreposearch.domain.entity.RepositoryItem;

/**
 * Created by kuba on 28/03/2018.
 */

public final class RepositoryItemMock {

    private RepositoryItemMock() {
        //no-op
    }

    public static RepositoryItem.Builder get() {
        return RepositoryItem.newBuilder()
                .withId(1234L)
                .withName("Name")
                .withDescription("Test description")
                .withStarCount(23L)
                .withOwner(UserItemMock.get().build());
    }
}
