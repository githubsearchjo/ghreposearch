package com.mobicubes.ghreposearch.presentation.userdetail.presenter

import com.mobicubes.ghreposearch.presentation.BasePresenter
import com.mobicubes.ghreposearch.presentation.userdetail.param.RepositoryDetailParam
import com.mobicubes.ghreposearch.presentation.userdetail.presenter.RepositoryDetailPresenter.State.Companion.VIEW_MODEL
import com.mobicubes.ghreposearch.presentation.userdetail.view.RepositoryDetailView
import com.mobicubes.ghreposearch.presentation.userdetail.view.viewmodel.RepositoryDetailViewModel
import java.io.Serializable
import java.util.*

/**
 * Created by kuba on 11/03/2018.
 */
class RepositoryDetailPresenter(
        val view: RepositoryDetailView,
        param: RepositoryDetailParam
) : BasePresenter {

    private var viewModel = RepositoryDetailViewModel(param.repository)

    override fun onCreate(stateMap: Map<String, Serializable>?) {
        if (stateMap != null) {
            val storedViewModel = stateMap[VIEW_MODEL] as RepositoryDetailViewModel?
            if (storedViewModel != null) viewModel = storedViewModel
        }
        view.setViewModel(viewModel)
    }

    override fun getState(): Map<String, Serializable> = Collections.singletonMap(State.VIEW_MODEL, viewModel)

    class State {
        companion object {
            val VIEW_MODEL = State::class.java.canonicalName + ".VIEW_MODEL"
        }
    }

    override fun onDestroy() {
        //no-op
    }

}