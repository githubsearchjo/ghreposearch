package com.mobicubes.ghreposearch.presentation

import java.io.Serializable

/**
 * Created by kuba on 12/03/2018.
 */
interface BasePresenter {
    fun onCreate(stateMap: Map<String, Serializable>?)
    fun onDestroy()
    fun getState(): Map<String, Serializable>
}