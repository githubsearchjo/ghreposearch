package com.mobicubes.ghreposearch.presentation.userdetail.param

import com.mobicubes.ghreposearch.domain.entity.RepositoryItem
import com.mobicubes.ghreposearch.domain.entity.UserItem
import java.io.Serializable

/**
 * Created by kuba on 11/03/2018.
 */
class RepositoryDetailParam(
        val repository: RepositoryItem
) : Serializable