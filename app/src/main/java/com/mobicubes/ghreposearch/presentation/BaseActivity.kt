package com.mobicubes.ghreposearch.presentation

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.mobicubes.ghreposearch.util.StateUtil

/**
 * Created by kuba on 12/03/2018.
 */
abstract class BaseActivity : AppCompatActivity(), BaseView {

    private var toast: Toast? = null

    override fun displayMessage(message: String) {
        toast?.cancel()
        toast = Toast.makeText(this, message, Toast.LENGTH_LONG)
        toast?.show()
    }

    abstract protected fun getPresenter(): BasePresenter

    override fun onSaveInstanceState(outState: Bundle?) {
        outState?.putAll(StateUtil.getBundle(getPresenter().getState()))
        super.onSaveInstanceState(outState)
    }

    override fun onDestroy() {
        super.onDestroy()
        getPresenter().onDestroy()
    }
}