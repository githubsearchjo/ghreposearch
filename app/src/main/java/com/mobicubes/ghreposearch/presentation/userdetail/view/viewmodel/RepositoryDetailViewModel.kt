package com.mobicubes.ghreposearch.presentation.userdetail.view.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import com.mobicubes.ghreposearch.BR
import com.mobicubes.ghreposearch.domain.entity.RepositoryItem
import com.mobicubes.ghreposearch.domain.entity.UserItem
import java.io.Serializable

/**
 * Created by kuba on 11/03/2018.
 */
class RepositoryDetailViewModel(
        val repositoryItem: RepositoryItem
) : BaseObservable(), Serializable {

    fun getStarsCountText() = "${repositoryItem.starCount}"

    fun getName() = repositoryItem.name

    fun getImageUrl() = repositoryItem.owner.avatarUrl

    fun getOwnerName() = repositoryItem.owner.login

    fun getDescription() = if (repositoryItem.description == null) "" else repositoryItem.description
}