package com.mobicubes.ghreposearch.presentation.search.view.viewmodel

import java.io.Serializable

/**
 * Created by kuba on 11/03/2018.
 */
interface ItemViewModel : Serializable {

    fun getId(): Long
    fun getType(): Type
    fun getTitle(): String
    fun clickable() = getType() == Type.REPOSITORY

    enum class Type { USER, REPOSITORY }
}