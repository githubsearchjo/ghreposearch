package com.mobicubes.ghreposearch.presentation.search.presenter

import com.mobicubes.ghreposearch.domain.entity.RepositoryItem
import com.mobicubes.ghreposearch.domain.interactor.GetUserRepositoriesUseCase
import com.mobicubes.ghreposearch.presentation.BasePresenter
import com.mobicubes.ghreposearch.presentation.search.view.SearchView
import com.mobicubes.ghreposearch.presentation.search.view.viewmodel.ItemViewModel
import com.mobicubes.ghreposearch.presentation.search.view.viewmodel.RepositoryViewModel
import com.mobicubes.ghreposearch.presentation.search.view.viewmodel.SearchViewModel
import com.mobicubes.ghreposearch.util.ErrorMapper
import io.reactivex.observers.DisposableObserver
import java.io.Serializable
import java.util.*

/**
 * Created by kuba on 11/03/2018.
 */

class SearchPresenter(
        val view: SearchView,
        private val getUserRepositoriesUseCase: GetUserRepositoriesUseCase
) : BasePresenter, SearchView.Listener {

    val USER_NAME = "square"

    var viewModel: SearchViewModel = SearchViewModel()

    override fun onCreate(stateMap: Map<String, Serializable>?) {
        view.setListener(this)

        if (stateMap != null) {
            val storedViewModel = stateMap[State.VIEW_MODEL] as SearchViewModel?
            if (storedViewModel != null) viewModel = storedViewModel
        }
        viewModel.inProgress.set(false)
        view.setViewModel(viewModel)
        if (viewModel.items.isEmpty()) {
            searchRepositories(USER_NAME)
        }
    }

    override fun onDestroy() {
        unsubscribeUseCases()
    }

    private fun unsubscribeUseCases() {
        getUserRepositoriesUseCase.unsubscribe()
    }

    override fun getState(): Map<String, Serializable> = Collections.singletonMap(State.VIEW_MODEL, viewModel)

    override fun onItemClick(item: ItemViewModel) {
        when (item) {
            is RepositoryViewModel -> view.goToRepositoryDetail(item.repository)
            else -> return
        }
    }

    override fun onQueryChanged(query: String) {
        // no-op - handled by viewModel
    }

    private fun displayError(e: Throwable) {
        view.displayMessage("Error: ${ErrorMapper.mapThrowable(e)}")
    }

    private fun updateViewModel(repositories: List<RepositoryItem>) {
        viewModel.items = (repositories.map { RepositoryViewModel(it) })
        viewModel.inProgress.set(false)
    }

    fun searchRepositories(query: String) {
        viewModel.inProgress.set(true)
        getUserRepositoriesUseCase.execute(query, object : DisposableObserver<List<RepositoryItem>>() {
            override fun onError(e: Throwable) {
                displayError(e)
                viewModel.inProgress.set(false)
            }

            override fun onComplete() {}

            override fun onNext(t: List<RepositoryItem>) {
                updateViewModel(t)
            }
        })
    }

    class State {
        companion object {
            val VIEW_MODEL = State::class.java.canonicalName + ".VIEW_MODEL"
        }
    }
}