package com.mobicubes.ghreposearch.presentation.userdetail.di;

import android.support.annotation.NonNull;

import com.mobicubes.ghreposearch.domain.interactor.GetFollowersCountUseCase;
import com.mobicubes.ghreposearch.domain.interactor.GetStarsCountUseCase;
import com.mobicubes.ghreposearch.presentation.userdetail.RepositoryDetailActivity;
import com.mobicubes.ghreposearch.presentation.userdetail.param.RepositoryDetailParam;
import com.mobicubes.ghreposearch.presentation.userdetail.presenter.RepositoryDetailPresenter;
import com.mobicubes.ghreposearch.presentation.userdetail.view.RepositoryDetailView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kuba on 11/03/2018.
 */

@Module
public class UserDetailActivityModule {

    private final RepositoryDetailActivity activity;

    public UserDetailActivityModule(@NonNull final RepositoryDetailActivity activity) {
        this.activity = activity;
    }

    @Provides
    public RepositoryDetailView provideView() {
        return activity;
    }

    @Provides
    public RepositoryDetailParam provideParam() {
        return activity.getExtra();
    }

    @Provides
    public RepositoryDetailPresenter providePresenter(
            @NonNull final RepositoryDetailView view,
            @NonNull final RepositoryDetailParam param
    ) {
        return new RepositoryDetailPresenter(view, param);
    }
}
