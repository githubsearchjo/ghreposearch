package com.mobicubes.ghreposearch.presentation.search.view.binding;

import android.databinding.BindingAdapter;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import com.mobicubes.ghreposearch.presentation.search.view.SearchView;
import com.mobicubes.ghreposearch.presentation.search.view.adapter.ResultAdapter;
import com.mobicubes.ghreposearch.presentation.search.view.viewmodel.ItemViewModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuba on 11/03/2018.
 */

public final class RecyclerViewBindingAdapter {

    private RecyclerViewBindingAdapter() {
        //no-op
    }

    @BindingAdapter({"items", "listener"})
    public static void setAdapter(
            @NonNull final RecyclerView recyclerView,
            @NonNull final List<ItemViewModel> items,
            @NonNull final SearchView.Listener listener
    ) {
        if (recyclerView.getAdapter() instanceof ResultAdapter) {
            ResultAdapter adapter = (ResultAdapter) recyclerView.getAdapter();
            adapter.getItems().clear();
            adapter.getItems().addAll(items);
            adapter.notifyDataSetChanged();
        } else {
            List<ItemViewModel> adapterList = new ArrayList<>();
            adapterList.addAll(items);
            recyclerView.setAdapter(new ResultAdapter(adapterList, listener));
        }
    }
}
