package com.mobicubes.ghreposearch.presentation.search.view.viewmodel

import android.databinding.BaseObservable
import android.databinding.Bindable
import android.databinding.ObservableBoolean
import com.mobicubes.ghreposearch.BR
import java.io.Serializable
import java.util.*

/**
 * Created by kuba on 11/03/2018.
 */
class SearchViewModel : BaseObservable(), Serializable {

    var inProgress = ObservableBoolean(false)

    var itemViewModels: List<ItemViewModel> = Collections.emptyList()
    var searchQuery = ""

    var items: List<ItemViewModel>
        @Bindable get() = itemViewModels
        set(value) {
            itemViewModels = value
            notifyPropertyChanged(BR.filteredResults)
        }

    var query: String
        @Bindable get() = searchQuery
        set(value) {
            searchQuery = value
            notifyPropertyChanged(BR.filteredResults)
        }

    @Bindable
    fun getFilteredResults() = if (query.isEmpty()) items else filterItems()

    fun filterItems() = items.filter { item -> item.getTitle().contains(query, true) }
}