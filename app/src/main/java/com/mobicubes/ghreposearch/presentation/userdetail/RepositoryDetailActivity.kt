package com.mobicubes.ghreposearch.presentation.userdetail

import android.databinding.DataBindingUtil
import android.os.Bundle
import com.mobicubes.ghreposearch.MyApplication
import com.mobicubes.ghreposearch.R
import com.mobicubes.ghreposearch.databinding.ViewUserDetailBinding
import com.mobicubes.ghreposearch.presentation.BaseActivity
import com.mobicubes.ghreposearch.presentation.BasePresenter
import com.mobicubes.ghreposearch.presentation.userdetail.di.UserDetailActivityModule
import com.mobicubes.ghreposearch.presentation.userdetail.param.RepositoryDetailParam
import com.mobicubes.ghreposearch.presentation.userdetail.presenter.RepositoryDetailPresenter
import com.mobicubes.ghreposearch.presentation.userdetail.view.RepositoryDetailView
import com.mobicubes.ghreposearch.presentation.userdetail.view.viewmodel.RepositoryDetailViewModel
import com.mobicubes.ghreposearch.util.StateUtil
import javax.inject.Inject

/**
 * Created by kuba on 11/03/2018.
 */
class RepositoryDetailActivity : BaseActivity(), RepositoryDetailView {

    companion object {
        val EXTRA = "USER_DETAIL_EXTRA"
    }

    private lateinit var binding: ViewUserDetailBinding

    @Inject lateinit var presenter: RepositoryDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        (applicationContext as MyApplication).appComponent
                .plus(UserDetailActivityModule(this))
                .inject(this)

        binding = DataBindingUtil.setContentView(this, R.layout.view_user_detail)

        presenter.onCreate(StateUtil.getStateMap(savedInstanceState))
    }

    override fun getPresenter(): BasePresenter = presenter

    fun getExtra() = intent.getSerializableExtra(EXTRA) as RepositoryDetailParam

    override fun setViewModel(viewModel: RepositoryDetailViewModel) {
        binding.viewModel = viewModel
    }
}