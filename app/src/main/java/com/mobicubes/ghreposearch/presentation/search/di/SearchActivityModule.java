package com.mobicubes.ghreposearch.presentation.search.di;

import android.support.annotation.NonNull;

import com.mobicubes.ghreposearch.domain.interactor.GetUserRepositoriesUseCase;
import com.mobicubes.ghreposearch.presentation.search.SearchActivity;
import com.mobicubes.ghreposearch.presentation.search.presenter.SearchPresenter;
import com.mobicubes.ghreposearch.presentation.search.view.SearchView;

import dagger.Module;
import dagger.Provides;

/**
 * Created by kuba on 11/03/2018.
 */

@Module
public class SearchActivityModule {

    private final SearchActivity activity;

    public SearchActivityModule(@NonNull final SearchActivity activity) {
        this.activity = activity;
    }

    @Provides
    public SearchView provideView() {
        return activity;
    }

    @Provides
    public SearchPresenter providePresenter(
            @NonNull final SearchView view,
            @NonNull final GetUserRepositoriesUseCase getUserRepositoriesUseCase
    ) {
        return new SearchPresenter(
                view,
                getUserRepositoriesUseCase
        );
    }
}
