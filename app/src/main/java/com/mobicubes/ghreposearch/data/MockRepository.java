package com.mobicubes.ghreposearch.data;

import android.support.annotation.NonNull;

import com.mobicubes.ghreposearch.domain.entity.RepositoryItem;
import com.mobicubes.ghreposearch.domain.entity.UserItem;
import com.mobicubes.ghreposearch.domain.entity.mock.RepositoryItemMock;
import com.mobicubes.ghreposearch.domain.repository.Repository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by kuba on 28/03/2018.
 */

public class MockRepository implements Repository {
    @NonNull
    @Override
    public Observable<List<UserItem>> searchUsers(@NonNull String query) {
        return null;
    }

    @NonNull
    @Override
    public Observable<List<RepositoryItem>> searchRepositories(@NonNull String query) {
        return null;
    }

    @NonNull
    @Override
    public Observable<Long> getFollowersCount(@NonNull String login) {
        return null;
    }

    @NonNull
    @Override
    public Observable<Long> getUserRepositoriesStarCount(@NonNull String login) {
        return null;
    }

    @NonNull
    @Override
    public Observable<List<RepositoryItem>> getUserRepositories(@NonNull String login) {
        return null;
    }


}
