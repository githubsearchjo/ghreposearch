package com.mobicubes.ghreposearch.data;

import android.support.annotation.NonNull;

import com.mobicubes.ghreposearch.data.network.Service;
import com.mobicubes.ghreposearch.data.network.mapper.RepositoryMapper;
import com.mobicubes.ghreposearch.data.network.mapper.UserMapper;
import com.mobicubes.ghreposearch.domain.entity.RepositoryItem;
import com.mobicubes.ghreposearch.domain.entity.UserItem;
import com.mobicubes.ghreposearch.domain.repository.Repository;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;

/**
 * Created by kuba on 10/03/2018.
 */

public class RepositoryImpl implements Repository {

    private static final String API_TOKEN = "0242d52d98a5837471146a93f1bb70acced102d5";
    private static final Integer REPOSITORIES_PER_PAGE = 20;

    private static final Map<String, String> HEADER_MAP =
            Collections.singletonMap("Authorization", "token " + API_TOKEN);

    @NonNull
    private final Service service;

    public RepositoryImpl(@NonNull Service service) {
        this.service = service;
    }

    @NonNull
    @Override
    public Observable<List<UserItem>> searchUsers(@NonNull final String query) {
        return service.searchUsers(HEADER_MAP, query)
                .map(UserMapper::mapListFromUsersResponse);
    }

    @NonNull
    @Override
    public Observable<List<RepositoryItem>> searchRepositories(@NonNull final String query) {
        return service.searchRepositories(HEADER_MAP, query)
                .map(RepositoryMapper::mapListFromRepositoriesResponse);
    }

    @NonNull
    @Override
    public Observable<Long> getFollowersCount(@NonNull final String login) {
        return service.getFollowers(login)
                .map(UserMapper::mapListToCount);
    }

    @NonNull
    @Override
    public Observable<Long> getUserRepositoriesStarCount(@NonNull String login) {
        return service.getUserRepositories(HEADER_MAP, login, REPOSITORIES_PER_PAGE)
                .map(RepositoryMapper::mapListToStarsCount);
    }

    @NonNull
    @Override
    public Observable<List<RepositoryItem>> getUserRepositories(@NonNull String login) {
        return service.getUserRepositories(HEADER_MAP, login, REPOSITORIES_PER_PAGE)
                .map(RepositoryMapper::mapList);
    }
}
