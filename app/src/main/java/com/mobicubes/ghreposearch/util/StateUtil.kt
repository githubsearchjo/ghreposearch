package com.mobicubes.ghreposearch.util

import android.os.Bundle
import java.io.Serializable

/**
 * Created by kuba on 12/03/2018.
 */
class StateUtil {
    companion object {
        fun getStateMap(bundle: Bundle?): Map<String, Serializable>? {
            if (bundle == null) {
                return null
            }
            val stateMap = HashMap<String, Serializable>()
            bundle.keySet().forEach { stateMap.put(it, bundle.getSerializable(it)) }
            return stateMap
        }

        fun getBundle(state: Map<String, Serializable>?): Bundle? {
            if (state == null || state.isEmpty()) {
                return null
            }
            val bundle = Bundle()
            state.entries.forEach { bundle.putSerializable(it.key, it.value) }
            return bundle
        }
    }
}