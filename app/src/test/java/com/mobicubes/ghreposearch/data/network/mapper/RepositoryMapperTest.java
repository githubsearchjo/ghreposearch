package com.mobicubes.ghreposearch.data.network.mapper;

import com.mobicubes.ghreposearch.data.network.entity.RepositoriesResponseJson;
import com.mobicubes.ghreposearch.data.network.entity.RepositoryJson;
import com.mobicubes.ghreposearch.data.network.entity.UserJson;
import com.mobicubes.ghreposearch.domain.entity.RepositoryItem;
import com.mobicubes.ghreposearch.domain.entity.UserItem;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by kuba on 10/03/2018.
 */

public final class RepositoryMapperTest {

    private static final Long TEST_ID = 1234L;
    private static final String TEST_NAME = "Name";
    private static final String TEST_DESCRIPTION = "Test description";
    private static final Long TEST_STAR_COUNT = 23L;

    private static final String USER_AVATAR_URL = "avatar_url";
    private static final Long USER_TEST_ID = 2345L;
    private static final String USER_TEST_LOGIN = "login";

    private static final UserItem USER_ITEM = UserItem.newBuilder()
            .withAvatarUrl(USER_AVATAR_URL)
            .withId(USER_TEST_ID)
            .withLogin(USER_TEST_LOGIN)
            .build();

    private static final UserJson USER_JSON = UserJson.newBuilder()
            .withAvatarUrl(USER_AVATAR_URL)
            .withId(USER_TEST_ID)
            .withLogin(USER_TEST_LOGIN)
            .build();

    private static RepositoryJson.Builder provideTestJsonBuilder() {
        return RepositoryJson.newBuilder()
                .withId(TEST_ID)
                .withName(TEST_NAME)
                .withDescription(TEST_DESCRIPTION)
                .withStarCount(TEST_STAR_COUNT)
                .withOwner(USER_JSON);
    }

    private static RepositoryItem.Builder provideExpectedItemBuilder() {
        return RepositoryItem.newBuilder()
                .withId(TEST_ID)
                .withName(TEST_NAME)
                .withDescription(TEST_DESCRIPTION)
                .withStarCount(TEST_STAR_COUNT)
                .withOwner(USER_ITEM);
    }

    @Test
    public void shouldProperlyMapJsonToDomain() {

        final RepositoryJson repositoryJson = provideTestJsonBuilder().build();

        final RepositoryItem expected = provideExpectedItemBuilder().build();

        assertEquals(expected, RepositoryMapper.map(repositoryJson));
    }

    @Test
    public void shouldThrowExceptionForLackingId() {
        final RepositoryJson repositoryJson = provideTestJsonBuilder()
                .withId(null)
                .build();

        try {
            RepositoryMapper.map(repositoryJson);
            Assert.fail("Should throw proper exception");
        } catch (RuntimeException e) {
            // success
        }
    }

    @Test
    public void shouldThrowExceptionForLackingName() {
        final RepositoryJson repositoryJson = provideTestJsonBuilder()
                .withName(null)
                .build();

        try {
            RepositoryMapper.map(repositoryJson);
            Assert.fail("Should throw proper exception");
        } catch (RuntimeException e) {
            // success
        }
    }

    @Test
    public void shouldThrowExceptionForEmptyName() {
        final RepositoryJson repositoryJson = provideTestJsonBuilder()
                .withName("")
                .build();

        try {
            RepositoryMapper.map(repositoryJson);
            Assert.fail("Should throw proper exception");
        } catch (RuntimeException e) {
            // success
        }
    }

    @Test
    public void shouldThrowExceptionForLackingStarCount() {
        final RepositoryJson repositoryJson = provideTestJsonBuilder()
                .withStarCount(null)
                .build();

        try {
            RepositoryMapper.map(repositoryJson);
            Assert.fail("Should throw proper exception");
        } catch (RuntimeException e) {
            // success
        }
    }

    @Test
    public void shouldIgnoreInvalidItems() {
        final RepositoriesResponseJson repositoriesResponseJson =
                RepositoriesResponseJson.newBuilder()
                        .withTotalCount(6L)
                        .withIncompleteResults(false)
                        .withItems(Arrays.asList(
                                provideTestJsonBuilder().build(),
                                provideTestJsonBuilder().withId(null).build(),
                                provideTestJsonBuilder().build(),
                                provideTestJsonBuilder().withName("").build(),
                                provideTestJsonBuilder().withName(null).build(),
                                provideTestJsonBuilder().withStarCount(null).build(),
                                provideTestJsonBuilder().withOwner(null).build()
                        ))
                        .build();

        final List<RepositoryItem> expectedList = RepositoryMapper.mapListFromRepositoriesResponse(repositoriesResponseJson);

        Assert.assertArrayEquals(
                Arrays.asList(
                        provideExpectedItemBuilder().build(),
                        provideExpectedItemBuilder().build()
                ).toArray(),
                expectedList.toArray()
        );
    }

    @Test
    public void shouldProperlyMapStarsCount() {
        final List<RepositoryJson> repositoryJsonList = Arrays.asList(
                provideTestJsonBuilder().withStarCount(1L).build(),
                provideTestJsonBuilder().withStarCount(2L).build(),
                provideTestJsonBuilder().withStarCount(3L).build(),
                provideTestJsonBuilder().withStarCount(4L).build()
        );

        Assert.assertTrue(10L == RepositoryMapper.mapListToStarsCount(repositoryJsonList));
    }
}
