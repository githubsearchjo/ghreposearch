package com.mobicubes.ghreposearch.domain.interactor

import com.mobicubes.ghreposearch.data.MockRepository
import com.mobicubes.ghreposearch.domain.entity.RepositoryItem
import com.mobicubes.ghreposearch.domain.entity.mock.RepositoryItemMock
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import io.reactivex.schedulers.Schedulers
import org.junit.Test

/**
 * Created by kuba on 28/03/2018.
 */

class GetUserRepositoriesUseCaseTest {

    val scheduler = Schedulers.trampoline()

    @Test
    fun shouldReturnValidResponse() {

        //given
        val testSubscriber = TestObserver<List<RepositoryItem>>()
        val repository = object : MockRepository() {
            override fun getUserRepositories(login: String): Observable<List<RepositoryItem>>
                    = Observable.just(listOf(RepositoryItemMock.get().build()))
        }
        val useCase = GetUserRepositoriesUseCase(scheduler, scheduler, repository)

        //when
        useCase.execute("login", testSubscriber)

        //then
        testSubscriber.assertNoErrors()
        testSubscriber.assertValue(listOf(RepositoryItemMock.get().build()))
    }

    @Test
    fun shouldPropagateError() {

        //given
        val testSubscriber = TestObserver<List<RepositoryItem>>()
        val repository = object : MockRepository() {
            override fun getUserRepositories(login: String): Observable<List<RepositoryItem>>
                    = Observable.error<List<RepositoryItem>>(NullPointerException())
        }
        val useCase = GetUserRepositoriesUseCase(scheduler, scheduler, repository)

        //when
        useCase.execute("login", testSubscriber)

        //then
        testSubscriber.assertError(NullPointerException::class.java)
    }
}